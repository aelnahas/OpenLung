# Concept 7
## Radial Arm Depressor

### Notes:
- Improvements on concept 6 based on community feedback.
- Combines the pulley idea with a depressor and a flat basket to cradle the BVM.
- This removes friction/stress to the BVM
- Roller can be manually adjusted to change volume/pressure.
- Sticky medical tape (can’t remember the name) can be applied to the roller to assist retraction (filling BVM up)

### Issues:
- TBD
